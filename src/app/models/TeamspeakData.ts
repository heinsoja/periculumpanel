export interface TeamspeakData {
    address: string;
    port: string;
    password: string;
}
