export interface WowItemSetInformation {
  id: number;
  name: string;
  setBonuses: [{
    description: string;
    threshold: number;
  }],
  items: Array<number>;
}
