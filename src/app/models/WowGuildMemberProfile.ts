export interface WowGuildMemberProfile {
  character: {
    name: string;
    realm: string;
    battlegroup: string;
    class: number;
    race: number;
    gender: number;
    level: number;
    achievementPoints: number;
    thumbnail: string;
    guild: string;
    guildRealm: string;
    lastModified: number;
  },
  rank: number;
}
