export interface WowItemInformation {

  id: number;
  disenchantingSkillRank: number,
  description: string;
  name: string;
  icon: string;
  stackable: number;
  itemBind: number;
  bonusStats: Array<object>;
  //"itemSpells": [],
  buyPrice: number;
  itemClass: number;
  itemSubClass: number;
  containerSlots: number;
  weaponInfo: {
    damage: {
      min: number;
      max: number;
      exactMin: number;
      exactMax: number;
    },
    weaponSpeed: number;
    dps: number;
  },
  inventoryType: number;
  equippable: number;
  itemLevel: number;
  maxCount: number;
  maxDurability: number;
  minFactionId: number;
  minReputation: number;
  quality: number;
  sellPrice: number;
  requiredSkill: number;
  requiredLevel: number;
  requiredSkillRank: number;
  itemSource: Array<object>;
  baseArmor: number;
  hasSockets: boolean;
  isAuctionable: boolean;
  armor: number;
  displayInfoId: number;
  nameDescription: string;
  nameDescriptionColor: string;
  upgradable: boolean;
  heroicTooltip: boolean;
  context: string;
  bonusLists: Array<object>;
  availableContexts: Array<string>;
  bonusSummary: {
    defaultBonusLists: Array<object>;
    "chanceBonusLists": Array<object>;
    "bonusChances": Array<object>;
  },
  artifactId: number;

}
