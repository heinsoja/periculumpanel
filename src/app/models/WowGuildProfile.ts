import {WowGuildMemberProfile} from "./WowGuildMemberProfile";

export interface WowGuildProfile {
    lastModified: string;
    name: string;
    realm: string;
    battlegroup: string;
    level: number;
    side: number;
    achievementPoints: number;
    members: [WowGuildMemberProfile];
}
