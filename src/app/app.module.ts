import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { environment } from '../environments/environment';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

// Services
import { AuthService } from './services/auth.service';
import { TeamspeakService } from './services/teamspeak.service';
import { SettingsService } from './services/settings.service';
import { WclogsService } from './services/wclogs.service';

// Boostrap https://ng-bootstrap.github.io/#/getting-started
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

// Material https://material.angular.io/guide/getting-started
import { MaterialModule } from './material.module';

// Angularfire2 https://github.com/angular/angularfire2
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';


// Components
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { MainComponent } from './components/main/main.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import {BlizzardService} from "./services/blizzard.service";

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MainComponent,
    ToolbarComponent,
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    NgbModule.forRoot(),
    AngularFireModule.initializeApp(environment.firebase),
    MaterialModule
  ],
  providers: [
    AngularFireAuth,
    AngularFireDatabase,
    AuthService,
    TeamspeakService,
    SettingsService,
    WclogsService,
    BlizzardService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
