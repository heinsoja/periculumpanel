import { Component, OnInit } from '@angular/core';
import { AuthService } from './services/auth.service';
import {BlizzardService} from "./services/blizzard.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  loggedIn = false;

  constructor(public af: AuthService, public bs: BlizzardService) {
    this.bs.getItemSetInformation({
      itemsetid: 1060
    }).subscribe(data => {
      console.log(data);
    });
  }

  ngOnInit() {
    this.af.afAuth.authState.subscribe(user => {
      this.loggedIn = !!(user); // vorgeschlagen von WebStorm
    });
  }



}
