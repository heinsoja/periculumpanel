import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {GetGuildProfileRequest} from "./Requests/GetGuildProfileRequest";
import {Observable} from "rxjs/Observable";
import {WowGuildProfile} from "../models/WowGuildProfile";
import {GetItemInformationRequest} from "./Requests/GetItemInformationRequest";
import {WowItemInformation} from "../models/WowItemInformation";
import {GetItemSetInformationRequest} from "./Requests/GetItemSetInformationRequest";
import {WowItemSetInformation} from "../models/WowItemSetInformation";

/**
 * Service für die Blizzard API
 * https://dev.battle.net/io-docs
 */
@Injectable()
export class BlizzardService {
  private apiKey = environment.battleNet.apiKey;
  private BASE_URL = 'https://eu.api.battle.net/wow/';

  constructor(public http: HttpClient) { }

  /**
   * Holt sich die Basisinformationen über eine Gilde
   * @param {GetGuildProfileRequest} request Für den Request benötigte Informationen
   * @returns {Observable<WowGuildProfile>} Gibt ein Observable vom Typ WowGuildProfile zurück, in dem alle Informationen zu finden sind
   */
  getGuildProfile(request: GetGuildProfileRequest) {
    let formattedGuildname = BlizzardService.formatSpacedString(request.guildname);
    let requestUrl = `${this.BASE_URL}guild/${request.server}/${formattedGuildname}?fields=achievements%2Cchallenge%2Cmembers&locale=de_DE&apikey=${this.apiKey}`;
    return this.http.get<WowGuildProfile>(requestUrl);
  }

  /**
   * Holt sich alle Informationen über ein Item
   * @param {GetItemInformationRequest} request Für den Request benötigte Informationen
   * @returns {Observable<WowItemInformation>} Gibt ein Observable vom Typ WowItemInformation zurück, in dem alle Informationen zu finden sind
   */
  getItemInformation(request: GetItemInformationRequest) {
    let requestUrl = `${this.BASE_URL}item/${request.itemid}?locale=de_DE&apikey=${this.apiKey}`;
    return this.http.get<WowItemInformation>(requestUrl);
  }

  /**
   * Holt sich alle Informationen über ein ItemSet
   * @param {GetItemSetInformationRequest} request Für den Request benötigte Informationen
   * @returns {Observable<WowItemSetInformation>} Gibt ein Observable vom Typ WowItemSetInformation zurück, in dem alle Informationen zu finden sind
   */
  getItemSetInformation(request: GetItemSetInformationRequest) {
    let requestUrl = `${this.BASE_URL}item/set/${request.itemsetid}?locale=de_DE&apikey=${this.apiKey}`;
    return this.http.get<WowItemSetInformation>(requestUrl);
  }


  /**
   * Formatiert einen String, da Zeichen wie Leerzeichen nicht
   * in der API verwendet werden können
   * @param {string} str Der unformatierte String)
   * @returns {string} Formatierter Gildenname, der in der API verwendet werden kann
   */
  private static formatSpacedString(str: string) {
    let retVal = "";
    let splitted = str.split(' ');
    for (let part of splitted) {
      retVal += (retVal == '') ? part : ('%20' + part);
    }
    return retVal;
  }

}
