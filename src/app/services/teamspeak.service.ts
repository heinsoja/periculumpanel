import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import { TeamspeakData } from '../models/TeamspeakData';

/**
 * Service um Informationen über den Teamspeak Server zu erhalten
 */
@Injectable()
export class TeamspeakService {
  /**
   * Die Teamspeak Informationen
   */
  data: Observable<TeamspeakData>;

  /**
   * Konstruktor zum injecten
   * @param db Injected AngularFireDatabase für die Informationen
   */
  constructor(db: AngularFireDatabase) {
    this.data = db.object('/teamspeak').valueChanges() as Observable<TeamspeakData>;
  }

  /**
   * Gibt einen Verbindugnsstring zum TeamspeakServer zurück,
   * mit dem sich der Client direkt verbinden kann
   */
  getLink() {
    return "";
  }

}
