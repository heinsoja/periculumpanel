import { TestBed, inject } from '@angular/core/testing';

import { WclogsService } from './wclogs.service';

describe('WclogsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WclogsService]
    });
  });

  it('should be created', inject([WclogsService], (service: WclogsService) => {
    expect(service).toBeTruthy();
  }));
});
