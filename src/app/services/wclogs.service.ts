import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {environment} from '../../environments/environment';

/**
 * Service für WarcraftLogs Daten
 * https://www.warcraftlogs.com/v1/docs/
 */
@Injectable()
export class WclogsService {
  private BASE_URL = 'https://www.warcraftlogs.com:443/v1/';
  private apiKey = environment.wclogs.apiKey;

  constructor(public http: HttpClient) {
  }

  getClasses() {
    return this.http.get(this.BASE_URL + 'classes?api_key=' + this.apiKey);
  }

}
