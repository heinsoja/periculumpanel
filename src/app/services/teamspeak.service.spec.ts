import { TestBed, inject } from '@angular/core/testing';

import { TeamspeakService } from './teamspeak.service';

describe('TeamspeakService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TeamspeakService]
    });
  });

  it('should be created', inject([TeamspeakService], (service: TeamspeakService) => {
    expect(service).toBeTruthy();
  }));
});
