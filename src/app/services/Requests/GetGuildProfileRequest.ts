export interface GetGuildProfileRequest {
  server: string;
  guildname: string;
}
