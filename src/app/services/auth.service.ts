import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { AngularFireAuth } from 'angularfire2/auth';

/**
 * Service zur Authentifizierung der Nutzer
 */
@Injectable()
export class AuthService {

  /**
   * Konstruktor zum injecten
   * @param afAuth Injected AngularFireAuth zur Authentifizierung der Nutzer
   */
  constructor(public afAuth: AngularFireAuth) {}

  /**
   * Loggt einen Nutzer ein
   * @param email Email des Nutzers
   * @param password Passwort des Nutzers
   */
  login(email: string, password: string) {
    this.afAuth.auth.signInWithEmailAndPassword(email, password).then(info => {
    });
  }

  /**
   * Loggt den aktuell angemeldeten Nutzer aus
   */
  logout() {
    this.afAuth.auth.signOut();
  }

}
