import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import { PanelSettings } from '../models/PanelSettings';

@Injectable()
export class SettingsService {
  constructor(public db: AngularFireDatabase) {
  }

}
