import { Component, OnInit } from '@angular/core';
import {MatInputModule} from '@angular/material';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email: string;
  password: string;

  constructor(public af: AuthService) { }

  ngOnInit() {
  }

  private loginHandler() {
    this.af.login(this.email, this.password);
  }

}
